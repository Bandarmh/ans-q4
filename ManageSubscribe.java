
package sa.edu.kau.fcit.cpit252;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ManageSubscribe {
    
     Map<String, List<TweetWatcher>> listeners = new HashMap<>();

    public ManageSubscribe(String keyword, ManageSubscribe event) {
            this.listeners.put(keyword, new ArrayList<>());
    }

    public ManageSubscribe(String keyword) {
     this.listeners.put(keyword, new ArrayList<>());
        
    }

    public void subscribe(String keyword, TweetWatcher listener) {
        List<TweetWatcher> users = listeners.get(keyword);
        users.add(listener);
    }

    public void unsubscribe(String keyword, TweetWatcher listener) {
        List<TweetWatcher> users = listeners.get(keyword);
        users.remove(listener);
    }

    public void notify(String keyword) {
        List<TweetWatcher> users = listeners.get(keyword);
        for (TweetWatcher listener : users) {
            listener.update(keyword);
        }
    }

    void notify(String makkah, ManageSubscribe events) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    void subscribe(String Keyword) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

   
}
