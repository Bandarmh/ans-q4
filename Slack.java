
package sa.edu.kau.fcit.cpit252;

public class Slack extends TweetWatcher{
    
    private String keyword;
    public ManageSubscribe events;
    
    
    public Slack(String keyword) {
        this.keyword = keyword;
    }

    @Override
    public void notified(String notifation) {

        System.out.println("notified to " + notifation + ": Someone has performed " + events);
    }
    
}
